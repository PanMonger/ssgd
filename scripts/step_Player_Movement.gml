//invulnerable time
if(invuln > 0)
    invuln--;
//Movement
if keyboard_check(vk_nokey)
{
//I want a crawl down speed but for now just immediate stop
  //  if(speed > 0)
  //      speed-=.8;
  //  else{
        speed = 0;
        sprite_index = sprite_Player_Idle;//}
}
//Temp Keybind
else if(keyboard_check(ord('A')) || keyboard_check(ord('W')) || keyboard_check(ord('D')) || keyboard_check(ord('S')))
{
    //Get sprite from direction
    get_Sprite_Direction();
    
    //Update Strafe Speed.
    if(vspeed != 0)
        vspeed = get_VertStrafe_Speed();
    if(hspeed != 0)
        hspeed = get_HorzStrafe_Speed();
        
    //Dreaded multikey same time inputs!
    if (get_MultiKey_Pressed() == "A")
        {
            hspeed = -get_MoveSpeed();
            if(keyboard_check(ord('S')))
                vspeed = get_MoveSpeed();
            else
                vspeed = -get_MoveSpeed();
        }
    else if (get_MultiKey_Pressed() == "D")
        {
            hspeed = get_MoveSpeed();
            if(keyboard_check(ord('S')))
                vspeed = get_MoveSpeed();
            else
                vspeed = -get_MoveSpeed();
        }//End Dreaded multikey same time inputs!
    
    //Single Key Press    
    //Left
    if keyboard_check_pressed(ord('A'))
        hspeed = -get_MoveSpeed();
    //Right
    else if keyboard_check_pressed(ord('D')) 
        hspeed = get_MoveSpeed();   
    //Up
    else if keyboard_check_pressed(ord('W'))
        vspeed = -get_MoveSpeed();
    //Down
    else if keyboard_check_pressed(ord('S'))
        vspeed = get_MoveSpeed();
    
    //Key Releases 
    //Check if holding opposite/multiple directions with ONE release
    if keyboard_check_released(ord('A'))
    {
        if(keyboard_check(ord('D')))
            hspeed = get_MoveSpeed();
        else
            hspeed = 0;
    }  
    else if keyboard_check_released(ord('D'))
    {
        if(keyboard_check(ord('A')))
            hspeed = -get_MoveSpeed();
        else
            hspeed = 0;
    }   
    else if keyboard_check_released(ord('W'))
    {
        if(keyboard_check(ord('S')))
            vspeed = get_MoveSpeed();
        else
            vspeed = 0;
    }   
    else if keyboard_check_released(ord('S'))
    {
        if(keyboard_check(ord('W')))
            vspeed = -get_MoveSpeed();
        else
            vspeed = 0;
    }
}//end else

//Dash
if (keyboard_check_pressed(vk_space))
    action_Dash();


