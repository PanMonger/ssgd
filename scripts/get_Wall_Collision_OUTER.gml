//Single : Wall Collision
// Up or Down
if(other.angle == "North"){// && keyboard_check(ord("W"))){
    if(get_Direction() <= 315 && get_Direction() > 225)
        y += get_MoveSpeed() * 0.75;
    else if(get_Direction() <= 135 && get_Direction() > 45)
        y += get_MoveSpeed() * 1.25;
    else
        y += get_MoveSpeed();
}
else if(other.object_index == obj_Wall_South){// && keyboard_check(ord("S"))){
    if(get_Direction() <= 135 && get_Direction() > 45)
        y -= get_MoveSpeed() * 0.75;
    else if(get_Direction() <= 315 && get_Direction() > 225)
        y -= get_MoveSpeed() * 1.25;
    else
        y -= get_MoveSpeed();
}
//Left or Right
if(other.object_index == obj_Wall_West){// && keyboard_check(ord("A"))){
    if(get_Direction() <= 45 || get_Direction() > 315)
        x += get_MoveSpeed() * 0.75;
    else if(get_Direction() <= 225 && get_Direction() > 135)
        x += get_MoveSpeed() * 1.25;
    else
        x += get_MoveSpeed();
}
else if(other.object_index == obj_Wall_East){// && keyboard_check(ord("D"))){
    if(get_Direction() <= 225 && get_Direction() > 135)
        x -= get_MoveSpeed() * 0.75;
    else if(get_Direction() <= 45 || get_Direction() > 315)
        x -= get_MoveSpeed() * 1.25;
    else
        x -= get_MoveSpeed();
}
