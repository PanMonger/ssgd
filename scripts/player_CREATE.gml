//Main Character Stats
//Insert as a Create
global.Player_Alive = true;
HP = 5;
Life = 2;
Shield = 3;
invuln = 0;
moveSpeed = 5;
//update_Direction = 1;
Dash = true;
alive = true;

//Set up equipment
player_FireArm = instance_create(x,y,obj_Player_FireArm);
player_FireArm.depth = -1;
player_Sword = instance_create(x,y,obj_Player_Sword);
player_Sword.depth = -2;
player_Shield = instance_create(x,y,obj_Player_Shield);
player_Shield.depth = 1;
