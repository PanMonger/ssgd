//setSprite Ver.2 USING MOUSE POINT DIRECTION
if(get_Direction() <= 45 || get_Direction() > 315)
    sprite_index = sprite_Player_Right;
else if(get_Direction() <= 135 && get_Direction() > 45)
    sprite_index = sprite_Player_Up;
else if(get_Direction() <= 225 && get_Direction() > 135)
    sprite_index = sprite_Player_Left;
else if(get_Direction() <= 315 && get_Direction() > 225)
    sprite_index = sprite_Player_Down;
