if(get_Direction() <= 45 || get_Direction() > 315)
    obj_Player.x += get_MoveSpeed() * 0.75;
else if(get_Direction() <= 225 && get_Direction() > 135)
    obj_Player.x += get_MoveSpeed() * 1.25;
else
    obj_Player.x += get_MoveSpeed();
