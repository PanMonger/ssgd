//Helper functione resets delay Rate Of Fire
if (global.new_Enemy[num].fireArm == "Pistol")
    rof_Delay = irandom_range(5,15);
else if (global.new_Enemy[num].fireArm == "SMG")
    rof_Delay = irandom_range(0,2);
else if (global.new_Enemy[num].fireArm == "Shotgun")
    rof_Delay = irandom_range(20,40);
else if (global.new_Enemy[num].fireArm == "Rifle")
    rof_Delay = irandom_range(30,50);
else if (global.new_Enemy[num].fireArm == "RocketLauncher")
    rof_Delay = irandom_range(50,70);
