//What kind of wall Collision?
//OUTER MASK ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
if(other.object_index == obj_Wall_Mask){
    //Up-Down
    if(other.angle == "North" || other.angle == "South")
        y -= vspeed;
    //Left-Right
    else if(other.angle == "East" || other.angle == "West")
        x -= hspeed;
}
//CORNER MASK ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
else if(other.object_index == obj_Corner_Mask){
    if(other.angle == "NW"){
        if(y-50 <= other.y)
            y -= vspeed;
        if(x-50 <= other.x)
            x -= hspeed;
    }
    else if(other.angle == "NE"){
        if(y-50 <= other.y)
            y -= vspeed;
        if(x+50 >= other.x)
            x -= hspeed;
    }
    else if(other.angle == "SW"){
        if(y+50 >= other.y)
            y -= vspeed;
        if(x-50 <= other.x)
            x -= hspeed;
    }
    else if(other.angle == "SE"){
        if(y+50 >= other.y)
            y -= vspeed;
        if(x+50 >= other.x)
            x -= hspeed;
    }
}
//INNER MASK ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
else if(other.object_index == obj_Inner_Mask){
    //Up-Down
    if(other.angle == "North" || other.angle == "South")
        y -= vspeed;        
    //Left-Right
    else if(other.angle == "East" || other.angle == "West")
        x -= hspeed;
}
//EDGE MASK ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
else{
    if(other.angle == "NW"){
        if(y+40 <= other.y && keyboard_check(ord("S")))
            y -= vspeed;
        else if(x+40 <= other.x && keyboard_check(ord("D")))
            x -= hspeed;
    }
    else if(other.angle == "NE"){
        if(y+40 <= other.y && keyboard_check(ord("S")))
            y -= vspeed;
        else if(x-40 >= other.x && keyboard_check(ord("A")))
            x -= hspeed;
    }
    else if(other.angle == "SW"){
        if(y-40 >= other.y && keyboard_check(ord("W")))
            y -= vspeed;
        else if(x+40 <= other.x && keyboard_check(ord("D")))
            x -= hspeed;
    }
    if(other.angle == "SE"){
        if(y-40 >= other.y && keyboard_check(ord("W")))
            y -= vspeed;
        else if(x-40 >= other.x && keyboard_check(ord("A")))
            x -= hspeed;
    }
}
