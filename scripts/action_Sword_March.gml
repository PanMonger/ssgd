    if(keyboard_check(ord("W")))
        vspeed = -obj_Player.moveSpeed;
    else if(keyboard_check(ord("D")))
        hspeed = obj_Player.moveSpeed;
    else if(keyboard_check(ord("S")))
        vspeed = obj_Player.moveSpeed;
    else if(keyboard_check(ord("E")))
        vspeed = -obj_Player.moveSpeed;
    else if(keyboard_check(ord("W")) && keyboard_check(ord("D")))
    {
        vspeed = -obj_Player.moveSpeed;
        hspeed = obj_Player.moveSpeed;
    }
    else if(keyboard_check(ord("W")) && keyboard_check(ord("A")))
    {
        vspeed = -obj_Player.moveSpeed;
        hspeed = -obj_Player.moveSpeed;
    }
    else if(keyboard_check(ord("S")) && keyboard_check(ord("D")))
    {
        vspeed = obj_Player.moveSpeed;
        hspeed = obj_Player.moveSpeed;
    }
    else if(keyboard_check(ord("S")) && keyboard_check(ord("A")))
    {
        vspeed = obj_Player.moveSpeed;
        hspeed = -obj_Player.moveSpeed;
    }
