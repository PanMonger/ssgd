//Sword Side/Slot

//There are issues in this segment.

if(mouse_check_button(mb_right) && !visible)
    hold++;
    
if(mouse_check_button_released(mb_right) && !visible){
    //thrust
    if(hold > 5)
        image_angle = get_Direction();
    //swing
    else
        image_angle = get_Direction() + 50;
    //visible
    visible = true;
}
/*
if(!visible){
    x = obj_Player.x+(5*(cos(image_angle*(pi/180))));
    y = obj_Player.y-(5*(sin(image_angle*(pi/180))));
}
*/
if(visible){
    //thrust step
    if(hold > 5){
        x = obj_Player.x+((thrust*10)*(cos(image_angle*(pi/180))));
        y = obj_Player.y-((thrust*10)*(sin(image_angle*(pi/180))));
    }
    // swing step
    if(hold <= 5){
        x = obj_Player.x+((thrust*10)*(cos((image_angle-10)*(pi/180))));
        y = obj_Player.y-((thrust*10)*(sin((image_angle-10)*(pi/180))));
        image_angle = point_direction(obj_Player.x, obj_Player.y, x, y);
    }
    if(thrust < 5)
        thrust++;
    //!visible
    if(swingTime > 0)
        swingTime--;
    else{
        swingTime = 7;
        thrust = 1;
        hold = 0;
        visible = false;
    }
}

//Try 2... Still problematic
/*
else if(mouse_check_button_released(mb_right) && !visible){
    if(key_Hold > 5)
        image_angle = get_Direction();
    else if(get_SwingType() == "clockWise"){
        clockWise = true;
        image_angle = get_Direction() + 70;
    }
    else if(get_SwingType() == "counterClock"){
        counterClock = true;
        image_angle = get_Direction() - 70;
    }
    visible = true;
}
if(visible){
    if(key_Hold > 5){
        x = obj_Player.x+((stretch*5)*(cos(image_angle*(pi/180))));
        y = obj_Player.y-((stretch*5)*(sin(image_angle*(pi/180))));
    }
    //ClockWise
    else if(clockWise == true){
        x = obj_Player.x+((stretch*5)*(cos((image_angle-10)*(pi/180))));
        y = obj_Player.y-((stretch*5)*(sin((image_angle-10)*(pi/180))));
        image_angle = point_direction(obj_Player.x, obj_Player.y, x, y);
    }
    //CounterClockwise
    else if(counterClock == true){
        x = obj_Player.x+((stretch*5)*(cos((image_angle+10)*(pi/180))));
        y = obj_Player.y-((stretch*5)*(sin((image_angle+10)*(pi/180))));
        image_angle = point_direction(obj_Player.x, obj_Player.y, x, y);   
    }
    if(stretch < 6)
        stretch++;
}

if(swingTime > 0 && visible)
    swingTime--;
else if(visible){
    swingTime = 10;
    key_Hold = 0;
    stretch = 1;
    clockwise = false;
    counterClock = false;
    visible = false;
}
*/
//Old Sword Code
/*
if(mouse_check_button(mb_right) && !visible)
    key_Hold++;

else if(mouse_check_button_released(mb_right) && !visible)
{
    if((key_Hold > 5 && (get_Direction() <= 45 || get_Direction() > 315)) || (key_Hold > 5 && get_Direction() <= 225  && get_Direction() > 135))
        image_angle = (((point_direction(x,y,mouse_x,mouse_y)) + 180));
    else if(key_Hold > 5)
        image_angle = point_direction(x,y,mouse_x,mouse_y);
    else if(get_Direction() <= 90 || get_Direction() > 270)
        image_angle = point_direction(x,y,mouse_x,mouse_y) + 60;
    else 
        image_angle = point_direction(x,y,mouse_x,mouse_y) - 60;
    visible = true;
}

if(visible && swingTime > 0 && key_Hold > 5)
{
    action_SwordThrust();
}
else if(visible && swingTime > 0 && key_Hold < 5)
{
    action_SwordSwing();
}

else if(visible)
{
    visible = false;
    swingTime = 10;
    key_Hold = 0;
    speed = 0;
}
*/
