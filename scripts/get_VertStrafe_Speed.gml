if(vspeed <= 0 && keyboard_check(ord('W')))
{
    if(get_Direction() <= 315 && get_Direction() > 225)
        return -get_MoveSpeed() * 0.75;
    else if(get_Direction() <= 135 && get_Direction() > 45)
        return -get_MoveSpeed() * 1.25;
    return -get_MoveSpeed();
}
else if(vspeed >= 0 && keyboard_check(ord('S')))
{
    if(get_Direction() <= 135 && get_Direction() > 45)
        return get_MoveSpeed() * 0.75;
    else if(get_Direction() <= 315 && get_Direction() > 225)
        return get_MoveSpeed() * 1.25;
    return get_MoveSpeed();
}
