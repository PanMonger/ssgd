for(i=1;i<8;i++){

N_Mask = instance_create((150+(i*100)),50,obj_Wall_Mask);
N_Mask.depth = 2000;
N_Mask.angle = "North";

E_Mask = instance_create(1050,(150+(i*100)),obj_Wall_Mask);
E_Mask.depth = 2000;
E_Mask.angle = "East";

S_Mask = instance_create((150+(i*100)),1050,obj_Wall_Mask)
S_Mask.depth = 2000;
S_Mask.angle = "South";

W_Mask = instance_create(50,(150+(i*100)),obj_Wall_Mask);
W_Mask.depth = 2000;
W_Mask.angle = "West";

}
//Corners
NW_Corner_Mask = instance_create(50,50,obj_Corner_Mask)
NW_Corner_Mask.angle = "NW";
NW_Corner_Mask.depth = 2000;
NW_Corner_Mask.image_angle = 0;
NW_Corner_Mask.sprite_index = mask_Corner;

NE_Corner_Mask = instance_create(1050,50,obj_Corner_Mask)
NE_Corner_Mask.angle = "NE";
NE_Corner_Mask.depth = 2000;
NE_Corner_Mask.image_angle = 270;
NE_Corner_Mask.sprite_index = mask_Corner;

SW_Corner_Mask = instance_create(50,1050,obj_Corner_Mask)
SW_Corner_Mask.angle = "SW";
SW_Corner_Mask.depth = 2000;
SW_Corner_Mask.image_angle = 90;
SW_Corner_Mask.sprite_index = mask_Corner;

SE_Corner_Mask = instance_create(1050,1050,obj_Corner_Mask)
SE_Corner_Mask.angle = "SE";
SE_Corner_Mask.depth = 2000;
SE_Corner_Mask.image_angle = 180;
SE_Corner_Mask.sprite_index = mask_Corner;

//Inner
N_inner = instance_create(400,400,obj_Inner_Mask);
N_inner.angle = "North";
N_inner.depth = 2000;
N_inner.sprite_index = mask_Outer;
N_inner.image_angle = 180;

W_inner = instance_create(300,500,obj_Inner_Mask);
W_inner.angle = "West";
W_inner.depth = 2000;
W_inner.sprite_index = mask_Outer;
W_inner.image_angle = 270;

S_inner = instance_create(400,600,obj_Inner_Mask);
S_inner.angle = "South";
S_inner.depth = 2000;
S_inner.sprite_index = mask_Outer;
S_inner.image_angle = 0;

E_inner = instance_create(500,500,obj_Inner_Mask);
E_inner.angle = "East";
E_inner.depth = 2000;
E_inner.sprite_index = mask_Outer;
E_inner.image_angle = 90;

NW_inner = instance_create(300,400,obj_Edge_Mask);
NW_inner.sprite_index = mask_Inner_Edge;
NW_inner.angle = "NW";

NE_inner = instance_create(500,400,obj_Edge_Mask);
NE_inner.sprite_index = mask_Inner_Edge;
NE_inner.angle = "NE";
NE_inner.image_angle = 270;

SW_inner = instance_create(300,600,obj_Edge_Mask);
SW_inner.sprite_index = mask_Inner_Edge;
SW_inner.angle = "SW";
SW_inner.image_angle = 90;

SE_inner = instance_create(500,600,obj_Edge_Mask);
SE_inner.sprite_index = mask_Inner_Edge;
SE_inner.angle = "SE";
SE_inner.image_angle = 180;
