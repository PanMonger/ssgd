//Gets type and fires
if(bulletType == "Weak")
    newBullet = instance_create(x,y,obj_AI_WeakBullet);
else if(bulletType == "Medium")
    newBullet = instance_create(x,y,obj_AI_MediumBullet);
else if(bulletType == "Strong")
    newBullet = instance_create(x,y,obj_AI_StrongBullet);
else if(bulletType == "Special")
    newBullet = instance_create(x,y,obj_AI_SpecialBullet);
accuracy = get_AI_Accuracy(num);
    
newBullet.image_angle = point_direction(x, y, obj_Player.x+accuracy, obj_Player.y+accuracy);
newBullet.direction = point_direction(x+accuracy, y+accuracy, obj_Player.x, obj_Player.y);
newBullet.speed = get_AI_BulletSpeed(num);
//Get Sound
if(sprite_index == sprite_Pistol1)
    audio_play_sound(Pistol_Fire_PlaceHolder,2,false);
//else if(etc...
