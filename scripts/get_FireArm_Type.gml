if(ai_Type == "LightInfantry")
{
    select_Weapon = irandom_range(1,150);
       
    if(select_Weapon <= 50)
        return "SMG";
    else if(select_Weapon > 50 && select_Weapon <= 100)
        return "Rifle";
    else
        return "RocketLauncher";
}
else if(ai_Type == "MediumInfantry")
{
    return "SMG";
}
else if(ai_Type == "HeavyInfantry")
{
    return "Shotgun";
}
else
    return "Pistol";
