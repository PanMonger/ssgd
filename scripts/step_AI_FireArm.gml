//If killed
if(!drop){
    if(global.new_Enemy[num].HP < 1){
        global.new_Enemy[num].alive = false;
        //TODO for ammo drop
        //if([this]DeathAnimation)
        drop = true;
        //instance_destroy();
    }
    //Check Player Living Status
    //Garbage system by the by...
    if(global.Player_Alive){
    
        // breathing shake \\
        if(shake < 61){
            shake++;
            gun_shake += 0.2;
        }
        else if(shake < 121){
            shake++;
            gun_shake -= 0.2;
        }
        else
            shake = 0;
        // breating shake \\
    
        //Get gun angle/position
        image_angle = point_direction(global.new_Enemy[num].x,global.new_Enemy[num].y,obj_Player.x+gun_shake, obj_Player.y+gun_shake);
        x = global.new_Enemy[num].x+(25*(cos(image_angle*(pi/180))));
        y = global.new_Enemy[num].y-(25*(sin(image_angle*(pi/180))));
        if(obj_Player.x < global.new_Enemy[num].x)
            image_yscale = -1;
        else
            image_yscale = 1;
    
        //End gun angle/position
    
        //shoot
        if (reload_Delay > 0)
            reload_Delay--;
            
        else if(rof_Delay > 0 && currentAmmo > 0)
            rof_Delay--;
            
        else if (abs(((obj_Player.x - x) + (obj_Player.y - y))) < fireRange && currentAmmo > 0)
        //TODO SHOTGUN && !get_Spread())
        {
            action_AI_Fire();
            currentAmmo--;
            get_AI_rofDelay();
        }
        else if(currentAmmo < 1){
            get_AI_maxAmmo(num);
            get_AI_reloadDelay(num);
        }
    }    
}
else if(drop){
    if(!throw){
        speed = 10;
        image_angle = direction = random_range(direction-100, direction+100);
        throw = true;
    }
    else if(speed > 0){
        speed -= 1;
        if(mouse_x < obj_Player.x)
            image_angle += 15;
        else
            image_angle -= 15;
    }
    else if(speed = 0 && currentAmmo > 0)
        image_angle += 1;
    else
        speed = 0;

}
