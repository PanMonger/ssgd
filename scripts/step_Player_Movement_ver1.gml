//Movement
//Run Case **TODO
if keyboard_check(vk_nokey)
{
    hspeed = 0;
    vspeed = 0;
    sprite_index = sprite_Player_Idle;
//    moveSpeed = get_MoveSpeed();
}
else
{
    //Get speed modifier from direction
    get_Sprite_Direction();
//    moveSpeed = get_Strafe_Speed();
    
//PROBLEM: MOVE SPEED DOESNT AUTO UPDATE
    
    //Left  
    if keyboard_check_pressed(ord('A'))
    {
        hspeed = -moveSpeed;
    }

    //Right
    else if keyboard_check_pressed(ord('D')) 
    {
        hspeed = moveSpeed;
    }
    
    //Up
    else if keyboard_check_pressed(ord('W'))
    {
        vspeed = -moveSpeed;
    }
    
    //Down
    else if keyboard_check_pressed(ord('S'))
    {
        vspeed = moveSpeed;
    }
    
    //Key Releases
    
    if keyboard_check_released(ord('A'))
    {
        if(keyboard_check(ord('D')))
        {
            hspeed = moveSpeed;
        }
        else
        {
            hspeed = 0;
        }
    }
       
    else if keyboard_check_released(ord('D'))
    {
        if(keyboard_check(ord('A')))
        {
            hspeed = -moveSpeed;
        }
        else
        {
            hspeed = 0;
        }
    }
        
    else if keyboard_check_released(ord('W'))
    {
        if(keyboard_check(ord('S')))
        {
            vspeed = moveSpeed;
        }
        else
        {
            vspeed = 0;
        }
    }
        
    else if keyboard_check_released(ord('S'))
    {
        if(keyboard_check(ord('W')))
        {
            vspeed = -moveSpeed;
        }
        else
        {
            vspeed = 0;
        }
    }
}//end else


