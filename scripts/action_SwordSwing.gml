if(swingTime > 8)
{
    direction = image_angle;
    speed = 10;
}
else if(get_Direction() <= 90 || get_Direction() > 270)
{
    image_angle -= 15;
    direction = image_angle - 90;
    speed = 7;
}
else
{
    image_angle += 15;
    direction = image_angle + 90;
    speed = 7;
}

swingTime--;
