if(keyboard_check_pressed(ord('0')))
{
    fireArm = "RocketLauncher";
    get_FireArm();
    visible = true;
}
if(keyboard_check_pressed(ord('9')))
{
    fireArm = "Rifle";
    get_FireArm();
    visible = true;
}
if(keyboard_check_pressed(ord('8')))
{
    fireArm = "Shotgun";
    get_FireArm();
    visible = true;
}
if(keyboard_check_pressed(ord('7')))
{
    fireArm = "SMG";
    get_FireArm();
    visible = true;
}
if(keyboard_check_pressed(ord('6')))
{
    fireArm = "Pistol";
    get_FireArm();
    visible = true;
}
