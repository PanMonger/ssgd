get_FireArmAngle();
get_FireArmLocation();

if(rof_Delay > 0)
    rof_Delay--;

if (currentAmmo == 0)
{
    if (mouse_check_button_pressed(mb_left) && visible)
        action_Throw();
}
else if(get_FullAuto())
{
    if (mouse_check_button(mb_left) && rof_Delay < 1 && currentAmmo > 0)
        action_Fire();
}
else if (get_SemiAuto())
{
    
    if (mouse_check_button_pressed(mb_left) && rof_Delay < 1 && currentAmmo > 0)            
        action_Fire();
}
else if (get_Spread())
{
    if (mouse_check_button_pressed(mb_left) && rof_Delay < 1 && currentAmmo > 0)
        action_FireSpread();
}
