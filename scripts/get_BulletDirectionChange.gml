if(sprite_index == sprite_SpecialBullet){
    speedChange = 0.5
    directionChange = 2;
}
else if(sprite_index == sprite_StrongBullet){
    speedChange = 0;
    directionChange = 0;
}
else{   
    speedChange = 0.8;
    directionChange = 10;
}
//NE ~ SE && SW ~ NW    
if((direction <= 45 || direction > 315) || (direction <= 225 && direction > 135)){
    if((other.x > x && other.y > y) || (other.x > x && other.y > y))
        direction += directionChange;
    else if((other.x < x && other.y < y) || (other.x > x && other.y > y))
        direction -= directionChange;
}//END NE ~ SE && SW ~ NW

//SE ~ SW && NW ~ NE
else if((direction <= 315 && direction > 225) || (direction <= 135 && direction > 45)){
    if((other.x > x && other.y > y) || (other.x < x && other.y < y))
        direction -= directionChange;
    else if((other.x < x && other.y > y) || (other.x > x && other.y < y))
        direction += directionChange;
}//END SE ~ SW && SW ~ NW

speed -= speedChange;
image_angle = direction;
