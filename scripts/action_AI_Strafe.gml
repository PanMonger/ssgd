axis_Direction = point_direction(x, y, obj_Player.x, obj_Player.y);

if((axis_Direction >= 135) && (axis_Direction < 225) || (axis_Direction >= 325) && (axis_Direction < 45) )
{
    if(y < obj_Player.y)
        vspeed = moveSpeed;
    else if(y > obj_Player.y)
        vspeed = -moveSpeed;
}
else
{
    if(x < obj_Player.x)
        hspeed = moveSpeed;
    else if(x > obj_Player.x)
        hspeed = -moveSpeed;
}       
