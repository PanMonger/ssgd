if(get_Direction() <= 45 || get_Direction() > 315)
{
    if (swingTime > 5)
        image_angle -= 36;
    else
    {
        direction = image_angle - 55;
        speed = 9;
    }
}
else if(get_Direction() <= 225 && get_Direction() > 135)
{
    if (swingTime > 5)
        image_angle += 36;
    else
    {
        direction = image_angle + 55;
        speed = 9;
    }
}
else
{
    //nothing
}
swingTime--;
