if(hspeed <= 0 && keyboard_check(ord('A')))
{
    if(get_Direction() <= 45 || get_Direction() > 315)
        return -get_MoveSpeed() * 0.75;
    else if(get_Direction() <= 225 && get_Direction() > 135)
        return -get_MoveSpeed() * 1.25;
    return -get_MoveSpeed();
}
else if(hspeed >= 0 && keyboard_check(ord('D')))
{
    if(get_Direction() <= 225 && get_Direction() > 135)
        return get_MoveSpeed() * 0.75;
    else if(get_Direction() <= 45 || get_Direction() > 315)
        return get_MoveSpeed() * 1.25;
    return get_MoveSpeed();
}

