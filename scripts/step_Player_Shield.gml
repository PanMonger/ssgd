x = obj_Player.x
y = obj_Player.y

if(keyboard_check_pressed(vk_shift) && obj_Player.Shield > 0)
{
    obj_Player.Shield--;
    visible = true;
}

if(visible)
{
    if(pop > 30)
    {
        image_xscale+= 0.05;
        image_yscale+= 0.05;    
        pop--;
    }   
    else if (pop > 0)
        pop--;
    else
    {
        pop = 50;
        visible = false;
        image_xscale=0.1;
        image_yscale=0.1; 
    }
}
