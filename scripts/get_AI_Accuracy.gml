if (global.new_Enemy[num].fireArm == "Pistol")
    return random_range(0,15);
else if (global.new_Enemy[num].fireArm == "SMG")
    return random_range(0,30);
else if (global.new_Enemy[num].fireArm == "Shotgun")
    return random_range(0,20);
else if (global.new_Enemy[num].fireArm == "Rifle")
    return random_range(0,10);
else if (global.new_Enemy[num].fireArm == "RocketLauncher")
    return random_range(0,5);
