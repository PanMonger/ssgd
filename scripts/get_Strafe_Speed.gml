if(hspeed <= 0 && keyboard_check(ord('A')))
{
    if(get_Direction() <= 45 || get_Direction() > 315)
        return -get_MoveSpeed() * 0.5;
    else if(get_Direction() <= 225 && get_Direction() > 135)
        return -get_MoveSpeed() * 1.5;
    else
        return -get_MoveSpeed();
}
else if(hspeed >= 0 && keyboard_check(ord('D')))
{
    if(get_Direction() <= 225 && get_Direction() > 135)
        return get_MoveSpeed() * 0.5;
    else if(get_Direction() <= 45 || get_Direction() > 315)
        return get_MoveSpeed() * 1.5;
}
else
if(vspeed <= 0 && keyboard_check(ord('W')))
{
    if(get_Direction() <= 315 && get_Direction() > 225)
        return -get_MoveSpeed() * 0.5;
    else if(get_Direction() <= 135 && get_Direction() > 45)
        return -get_MoveSpeed() * 1.5;
    else
        return -get_MoveSpeed();
}
else if(vspeed >= 0 && keyboard_check(ord('S')))
{
    if(get_Direction() <= 135 && get_Direction() > 45)
        return get_MoveSpeed() * 0.5;
    else if(get_Direction() <= 315 && get_Direction() > 225)
        return get_MoveSpeed() * 1.5;
}
return get_MoveSpeed();
