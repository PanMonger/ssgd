//Helper functione resets delay Rate Of Fire
if (fireArm == "Pistol")
    rof_Delay = 0;
else if (fireArm == "SMG")
    rof_Delay = 3;
else if (fireArm == "Shotgun")
    rof_Delay = 30;
else if (fireArm == "Rifle")
    rof_Delay = 40;
else if (fireArm == "RocketLauncher")
   rof_Delay = 60;
