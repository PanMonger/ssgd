if keyboard_check(vk_nokey)
{
    speed = 0;
    sprite_index = sprite_Player_Idle;
}
else 
{
    //Sets Player Sprite via Point Direction
    get_Sprite_Direction();
    //Speed determined by Sprite Movement and Mouse Point
    moveSpeed = get_Strafe_Speed();
    
    if keyboard_check(ord('A'))
    {
        if keyboard_check(ord('D'))
            hspeed = moveSpeed;
        else
            hspeed = -moveSpeed;
            
    }
    if keyboard_check(ord('D'))
    {
        hspeed = moveSpeed;
    }
    if keyboard_check(ord('W'))
    {
        vspeed = -moveSpeed;
        if keyboard_check(ord('S'))
            vspeed = moveSpeed;
    }
    if keyboard_check(ord('S'))
    {
        vspeed = moveSpeed;
        if keyboard_check(ord('W'))
            vspeed = -moveSpeed;
    }
    

}
