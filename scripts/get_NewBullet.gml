get_FireArm_Coord();

if(get_BulletType() == "Weak"){
    newBullet = instance_create(x+val_x,y-val_y,obj_Player_Bullet);
    newBullet.sprite_index = sprite_WeakBullet;}
else if(get_BulletType() == "Medium"){
    newBullet = instance_create(x+val_x,y-val_y,obj_Player_Bullet);
    newBullet.sprite_index = sprite_MediumBullet;}
else if(get_BulletType() == "Strong"){
    newBullet = instance_create(x+val_x,y-val_y,obj_Player_Bullet);
    newBullet.sprite_index = sprite_StrongBullet;}
else if(get_BulletType() == "Special"){
    newBullet = instance_create(x+val_x,y-val_y,obj_Player_Bullet);
    newBullet.sprite_index = sprite_SpecialBullet;}
    
newBullet.obj_Size = 2;
