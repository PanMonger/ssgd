//CreateBullet and Fire
newBullet = instance_create(x,y,obj_AI_FireArm);
newBullet.sprite_index = get_FireArmIndex();
newBullet.image_angle = obj_Player_FireArm.image_angle;
if(mouse_x < obj_Player.x)
    image_yscale = -1;
else
    image_yscale = 1;
newBullet.direction = newBullet.image_angle;
newBullet.speed = get_FireArmWeight();
newBullet.obj_Size = 10;
newBullet.gun_shake = 0;
newBullet.drop = true;
newBullet.throw = true;
newBullet.currentAmmo = 0;
newBullet.fireArm = fireArm;
//Hide FireArm (Easier then destroying)
visible = false;
