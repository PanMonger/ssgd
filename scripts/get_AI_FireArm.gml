enemy_Gun.fireArm = global.new_Enemy[i].fireArm;

if (global.new_Enemy[i].fireArm == "Pistol")
//Pistol
{
    enemy_Gun.currentAmmo = 9;
    enemy_Gun.bulletType = "Medium";
    enemy_Gun.reload_Delay = 20;
    enemy_Gun.fireRange = 1500;
    enemy_Gun.sprite_index = sprite_Pistol1;
}
else if (global.new_Enemy[i].fireArm == "SMG")
//SMG
{
    enemy_Gun.currentAmmo = 32;
    enemy_Gun.bulletType = "Weak";
    enemy_Gun.reload_Delay = 30;
    enemy_Gun.fireRange = 1500;
    enemy_Gun.sprite_index = sprite_SMG1;
}
else if (global.new_Enemy[i].fireArm == "Shotgun")
//Shotgun
{
    enemy_Gun.currentAmmo = 2;
    enemy_Gun.bulletType = "Weak";
    enemy_Gun.reload_Delay = 15;
    enemy_Gun.fireRame = 150;
    enemy_Gun.sprite_index = sprite_Shotgun1;
}
else if (global.new_Enemy[i].fireArm == "Rifle")
//Rifle
{
    enemy_Gun.currentAmmo = 3;
    enemy_Gun.bulletType = "Strong";
    enemy_Gun.reload_Delay = 30;
    enemy_Gun.fireRange = 1000;
    enemy_Gun.sprite_index = sprite_Rifle1;
}
else if (global.new_Enemy[i].fireArm == "RocketLauncher")
//Rocket
{
    enemy_Gun.currentAmmo = 1;
    enemy_Gun.reload_Delay = 60;
    enemy_Gun.bulletType = "Special";
    enemy_Gun.fireRange = 1000;
    enemy_Gun.sprite_index = sprite_RocketLauncher1;
}
    enemy_Gun.rof_Delay = 0;
