//This will need to be changed once real sprites introduced.
distance = noone;
angle = noone;
to_rad = pi/180;
to_deg = 180/pi;

//TODO |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

if(fireArm == "Pistol"){ //Handle: 7,15 | Barrel: 50,3
    distance = 35;
    //arctan(abs(y1-y2)/abs(x1-x2))
    angle = image_angle + (arctan(12/43)*to_deg);
    //angle2 -- Handle: 7,3 | Barrel: 50, 15
    angle2 = image_angle + (arctan(12/-43)*to_deg);
}
else if(fireArm == "SMG"){ //Get the numbers
    distance = 0;
    angle = image_angle + (arctan(0)*to_deg);
}
else if(fireArm == "Rifle"){ //Get the numbers
    distance = 0;
    angle = image_angle + (arctan(0)*to_deg);
}
else if(fireArm == "Shotgun"){ //Get the numbers
    distance = 0;
    angle = image_angle + (arctan(0)*to_deg);
}
else if(fireArm == "RocketLauncher"){ //Get the numbers
    distance = 0;
    angle = image_angle + (arctan(0)*to_deg);
}
//Check ImageFlip

if(obj_Player_FireArm.image_yscale == -1){
    val_y = (distance*(sin(angle2*to_rad)));
    val_x = distance*(cos(angle2*to_rad));
}
else{
    val_y = distance*(sin(angle*to_rad));
    val_x = distance*(cos(angle*to_rad));
}


